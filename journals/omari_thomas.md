THE JOURNAL STORY

01/16/24
DAY 1

Story: We woke to find ourselves stranded on a deserted island. There were four of us myself, Lily, Noah and Daniel.
We quickly introduced ourselves and formed a bond, to survive was our main goal but secretly, I could see the spirit
to thrive behind their eyes.

Task: Successfully worked with team to organize files, get in sync, determine roles and organize game plan.

01/17/24
DAY 2

Story: We climbed to the highest point on this island. We needed to get a sense for the terrain and organize
our thoughts for survival. The ocean seemed so vast around us, waves crashed on all sides as if to stifle any
ray of hope.

Task: We had to rehash or wireframe. There were a few things we did not accommodate for in our original plan.

01/18/24
DAY 3

Story: The chill of the wind was particularly cruel today. After our scouting on the previous day we noticed
a small forest at the back of the island and last night, strange noise emerged from that direction. We decided
we needed a sense of security and tried creating some weapons and structural tents. We did not fully succeed this
day.

Task: Cleaned up databases migrations adding more tables and updating previous rows. Setup PGAdmin and started
working on endpoints for authentication...

01/19/24
DAY 4

Story: A shriek from the depths of the forest woke us. Frantically we huddled together for a sense of security.
As we found solace in the embrace of company we all noticed, simultaneously, Daniel was nowhere to be found.
Our guts churned and we made sure to create our weapons, pronto.

Task: We successfully created our backend authentication and discussed plans for the coming week.

01/22/24
DAY 5

Story: We decided to set up landmarks at the spots we most enjoyed on the island, of course, ignoring the forest
for now.

Task: Successfully created endpoints for liked episode (get, delete, create).

01/23/24
DAY 6

Story: We not only marked familiar locations but we created maps using landmarks and learned how to tell direction
based on our shadows. Lily is proficient with making sure the structural integrity of the things we created is secure
and Noah forever amazes us bringing back meat when I can only catch fish.

Task: We created endpoints for comments, tell_us_anything and did a overall review and update on all endpoints.

01/24/24-01/26/24
DAY 7-9

Story: We each had our own personal goals but know the island is an unforgiving place, so we needed to get stronger.
For the next few days we did strength training and weapon mastery.

Task: Studied figma, pytesting and algorithms

01/29/24
DAY 10

Story: We finally decided to arm up and venture into the forest to the North. Our breaths were in sync and we trusted
each other. We believed nothing could stand in our way. We achieved success. I managed to help Noah with hunting a boar
and Lily immediately started roasting it.

Task: We completed auth for frontend and now have a working webpage that uses redux store.

01/30/24
DAY 11

Story: The chill of the morning woke me up. It had crept in and saturated my tent. I suspect the culprit is a rogue
squirrel messing with the leather I had used to cover my tent. Despite the distraction in the back of mind I
ventured into the forest for yet another day with the team. Everyone's face were briming with cheer. They were
eager to start the hunt. The camaraderie within the group was infectious, and soon my initial squirrel worries were
replaced with excitement. We followed a narrow trail that wound its way through the dense foliage, each step bringing
us closer to the unknown challenges that awaited us.

Task: worked on Readme and some frontend.

01/31/24
DAY 12

Story: I tumbled down a slope and got separated from the team. However, I was stil able to bring home a game to the camp.

Task: Aded a episode page that was carded and introduced the querysplicing from redux.

02/01/24
DAY 13

Story: Finally coming up on a rest day

Task: Trouble shoot error in console logs and updated readme

02/05/25
DAY 14

Task: Created liked episode page
