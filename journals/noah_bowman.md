This is my journal.

18JAN2024
Setup databases with out migrations. Added automatic UTC timestamp to comment creation table. Setup PGAdmin.
Cleaned up admin routes code.

17JAN2024
Today we finalized our database structure. That took forever. OMG. But we have it and it's good.

16JAN2024
Today we setup the project on Gitlab, got the PostgresQL BD setup and got everything running on Github.
I then added the third party API code that I had completed last week and made sure it was working.

09FEB2024
Today I was remembered that journals exist. A lot has happened. We've completed the API, Authentication, Frontend and our MVP is complete.

For me, my standout stuff was the API for Spotify and Apple, working out the DB stuff, and the liked_episode coding stuff(logic and animation) and also various bug fixes. We all worked on the whole project and I am INCREDIBLY happy with the teammates that I have been randomly assigned. I am blessed.

Today we are doing our presentation and and continuing to try and successfully deploy.

***
GOOD NEWS!!!  We successfully deployed.  Unfortunately, none of the photos can be hosted on gitlab so we have to find another place that the photos lives and hotlink them from there.

But our project lives!
